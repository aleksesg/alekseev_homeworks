**Домашние задания:**
- [05.Вывод минимальной цифры из последовательности;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/05/homework05)
- [06.Поиск индекс числа в массиве. Перемещение значимых элементов влево;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/06/homework06)
- [07.Поиск минимальной последовательности, сложность O(n);](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/07/homework07)
- [08.Создание объектов в виде массива людей, и вывод их по весу;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/08/src/homework08)
- [09.Задание по применению полиморфизма в наследовании;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/09/src/homework09)
- [10.Задание по применеию интерфейса;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/10/src/homework10)
- [11.Задание по применеию паттернa Singleton;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/11/src/homework11)
- 12.Задания нет. Не веришь посмотри [здесь;](https://gitlab.com/maxima_it_school/pcs_java_21_02/-/blob/master/Homeworks.md)
- [13.Задание на анонимные классы, используя лямбда выражения;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/13/src/homework13)
- 14.Задания нет;
- 15.Задания нет;
- [16.Задание на ArrayList и LinkedList;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/16/src/homework16)
- [17.Задание на иcпользование интерфейса Map;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/17/src/homework17)
- 18.Задания нет;
- [19.Задание на потоки ввода вывода. Реализовать методы UsersRepository;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/19/src/homework19)
- [20.Задание на иcпользование возможностей Stream API;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/20/src/homework20)
- [21.Реализовать многопоточное суммирование элементов;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/21/src/homework21)
- [21.Реализовать многопоточное суммирование элементов;](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/homework/21/src/homework21)
- 22.Задания нет;
- 23.Задания нет;


**Аттестационные задания:**
- [01.Аттестационное задание №1](https://gitlab.com/aleksesg/alekseev_homeworks/-/tree/attestation01-OOP/src/attestation/attestation01_OOP)
